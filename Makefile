
NAME = libftprintf.a

FL = -Wall -Wextra -Werror

SRC = ft_printf.c ft_putstr2.c \
	ft_print_mod.c ft_handle_fl_str.c \
	ft_itoa.c ft_putstr_n.c \
	ft_parse.c ft_handle_fl_num.c \
	ft_itoa_base.c ft_add_fl.c \
	ft_sv_fl.c wchar.c ft_strlen_wchr.c \
	ft_atoi.c ft_isdigit.c ft_putchar.c \
	ft_putstr.c ft_strchr.c ft_bzero.c \
	ft_strlen.c


OBJ = ft_printf.o ft_putstr2.o \
	 ft_print_mod.o ft_handle_fl_str.o \
	 ft_itoa.o ft_putstr_n.o \
	 ft_parse.o ft_handle_fl_num.o \
	 ft_itoa_base.o ft_add_fl.o \
	 ft_sv_fl.o wchar.o ft_strlen_wchr.o \
	 ft_atoi.o ft_isdigit.o ft_putchar.o \
	 ft_putstr.o ft_strchr.o ft_bzero.o \
	 ft_strlen.o

all: $(NAME)

$(NAME):
	gcc -c $(FL) $(SRC)  
	ar rc $(NAME) $(OBJ)
clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all