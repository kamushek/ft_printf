/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_fl.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 18:05:04 by mkurchin          #+#    #+#             */
/*   Updated: 2017/03/29 18:05:48 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_add_fl(char *fl, int *p , t_var *ptr)
{
	if (*fl == '0' && *p == '-')
		return ;
	else if (*fl == '0')
		ptr->zero = *fl;
	else if (*fl == ' ')
		ptr->space = ' ';
	else if (*fl == '#')
		ptr->diez = '#';
	else if (*fl == 'D')
	{	
		ptr->length = 'l';
		ptr->type = 'd';
	}
	else if (*fl == 'O')
	{
		ptr->length = 'l';
		ptr->type = 'o';
	}
	else if (*fl == 'U')
	{
		ptr->length = 'l';
		ptr->type = 'u';
	}
    else if (*fl == '+')
        ptr->plus = '+';
    else
	    *p = *fl;
}
