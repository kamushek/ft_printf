/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_sf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/23 19:00:01 by mkurchin          #+#    #+#             */
/*   Updated: 2017/03/23 19:00:02 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int ft_handl_wdth_str(t_var *ptr, int len, char chr)
{
		int sum;
		int i;
		
		i = -1;
		sum = (ptr->width < len) ? 0 : ptr->width - len;
		while (++i < sum)
			write(1, &chr, 1);
		return (i);
}

int ft_handl_precis_str(t_var *ptr, int len)
{
	int precis_len;

	precis_len = 0;
	precis_len = (len < ptr->precision) ? len : ptr->precision  ;
	return (precis_len);
}

int ft_handle_fl_str(void *arg, t_var *ptr)
{
	int len_byte;
	int len_chr;
	int print_chr;

	print_chr = 0;
	len_chr = 0;
	if (ptr->type != 0 && ptr->type != 'C')
	{
		len_byte = ft_strlen_wchr(arg, ptr);
        if(ptr->type == 'S')
            print_chr += len_byte;
		len_chr = len_byte;
	}
    else
        len_chr = 1;
	//printf("\n+++%i\n",(int)ft_strlen_wchr(arg, ptr));
	//printf("\n+++%i\n",(int)ft_strlen_wchr(arg, ptr));
	if (ptr->flag == '-')
	{
		if (ptr->length == 'h')
		{
			ft_handl_precis_str(ptr, len_chr);
			print_chr += ft_putstr_n(arg, len_byte, ptr);
			if(ptr->width != 0)
				print_chr += ft_handl_wdth_str(ptr, len_chr, ' ');
		}
    	else
		{
            if (ptr->dote != 0)
			    len_byte = ft_handl_precis_str(ptr, len_chr);
            print_chr += ft_putstr_n(arg, len_byte, ptr);
			if(ptr->width != 0)
                print_chr += ft_handl_wdth_str(ptr, len_byte, ' ');
		}
	}
	else if (ptr->flag == '0')
	{
		ft_handl_precis_str(ptr, len_chr);
		if(ptr->width != 0)
			ft_handl_wdth_str(ptr, len_chr, '0');
		ft_putstr_n(arg, len_byte, ptr);
	}
	
	if (ptr->flag == '+')
	{
		ft_putstr((char*)arg);
	}
	if (ptr->flag == ' ')
	{
		print_chr += ft_putstr_n(arg, len_chr, ptr);

	}
    if (ptr->flag == 0)
	{
		if (ptr->precision != 0)
			len_chr = ft_handl_precis_str(ptr, len_chr);
		if (ptr->width != 0 && ptr->zero == 0)
			print_chr += ft_handl_wdth_str(ptr, len_chr, ' ');
		else if (ptr->width != 0 && ptr->zero == '0')
			print_chr += ft_handl_wdth_str(ptr, len_chr, '0');
        print_chr += ft_putstr_n(arg, len_chr, ptr);
	}
	return (print_chr);
}