/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/07 18:44:32 by mkurchin          #+#    #+#             */
/*   Updated: 2017/04/07 18:44:37 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <stdarg.h>

void ft_test(char *chr, ...)
{
	void *arg;

	va_list ap;
	va_start(ap, chr);
	arg = va_arg(ap, void*);
	int bb = (int)arg;
    printf("%i\n", bb );               
	va_end(ap);

}
int		main(void)
{
	char *chr = "abse";
	int b = 123;
		ft_test(chr,b);
	return (0);
}
