#include "ft_printf.h"
#include <stdarg.h>
#include <stdio.h>

char			*ft_strrjoin(char *s1, char const *s2)
{
	char	*s3;
	int		i;
	size_t	len;

	len = ft_strlen(s1) + ft_strlen(s2);
	if (!(s3 = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	i = 0;
	if (s1)
	{
		while (s1[i])
			*s3++ = s1[i++];
		free(s1);
		s1 = NULL;
	}
	if (s2)
	{
		while (*s2)
		{
			*s3++ = *s2++;
			i++;
		}
	}
	*s3 = '\0';
	return (s3 -= i);
}