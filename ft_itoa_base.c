/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 17:12:05 by mkurchin          #+#    #+#             */
/*   Updated: 2017/03/30 17:12:07 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*ft_itoa_base(uintmax_t arg, int base, t_var *ptr)
{
	int			i;
	unsigned long long	value;
    unsigned long long	num;

    char		*s;
	char		*hex;
    unsigned int num_hex;

    num_hex = 0;
	hex = (ptr->type == 'x') ? "0123456789abcdef" : '\0';
	hex = (ptr->type == 'X') ? "0123456789ABCDEF" : "0123456789abcdef";
    //ft_cast_long(arg, *ptr)

//    num =(unsigned long long)(ft_is_unsign_type(ptr) == 0) ? ((long long)arg) *= -1 : (unsigned long long)arg;
    //i = ((ptr->sign == 1 && base == 10 && ft_is_unsign_type(ptr) == 0)) ? 2 : 1;
    i = 1;
    value = arg;
    num = value;
  	while (num /= base)
		i++;
	if ((s = (char*)malloc(sizeof(char) * i + 1)) == 0)
		return (NULL);
	s[i] = 0;

    // s[0] = (ptr->sign == 1 && base == 10) ? '-' : 0;


    s[0] = (value == 0) ? '0' : s[0];
    num = value;
	while (num)
	{
		//s[--i] = hex[(ptr->sign == 1) ? -(num % base) : num % base];
        s[--i] = hex[num % base];

        num /= base;
	}
    //printf("ASDAS%s\n",s);
	return (s);
}
