
#include <stdio.h>
 
int sum(int a, int b) {
    return a + b;
}
 
int maxx(int a, int b) {
    return (a > b)? a: b;
}
 
int mul(int a, int b) {
    return a*b;
}
 
void fold(int *arr, unsigned size, int (*fun)(int, int), int *acc)
{
    unsigned i;
    *acc = fun(arr[0], arr[1]);
    for (i = 2; i < size; i++) {
        *acc = fun(*acc, arr[i]);
    }
}
 
int main () {
    int a[] = {1, 2, 3, 4, 5, 10, 9, 8, 7, 6};
    int result;
     
    fold(a, 10, sum, &result);
    printf("%d\n", result);
    fold(a, 10, maxx, &result);
    printf("%d\n", result);
    fold(a, 10, mul, &result);
    printf("%d\n", result);
 
 return (0);   
}