/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <mkurchin@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 22:02:12 by mkurchin          #+#    #+#             */
/*   Updated: 2016/11/30 18:52:36 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
int     ft_cnt_prcnt(const char **s)
{
    int i;
    int j;
    const char *ptr;

    ptr = *s;
    j = 0;
    i = 0;
    while (*ptr)
    {
        if(*ptr != '%')
            break;
        i++;
        ptr++;
    }
    j = ((i % 2) == 0) ? i / 2 : (i - 1) / 2;
    while (j)
    {
        write(1, "%", 1);
        j--;
    }
    return (i);
}
int		ft_putstr2(const char **s, t_var *ptr)
{
    size_t len;
	int i;
    int fl;

    i = 0;
	fl = 0;
	char *str = (char*)*s;

    len = ft_strlen_wchr((void *)str, ptr);

	while (**s != '\0')
	{
		fl = 0;
		if(**s == '%' && *(*s+1) == '%')
		{

            i = ft_cnt_prcnt(s);
            (*s) += (i % 2 != 0) ? i - 1 : i;
            i = ((i % 2) == 0) ? i / 2 : (i - 1) / 2;

        }
        /*if(**s == '%' && *(*s+1) == '\0')
        {
            write(1, *s, 1);
            i++;
            break;
        }
      *//*  if(**s == '%' && *(*s-1) != '%')
        {
           if(!(ft_srch(((*s)+1),ptr)))
               break;
        }*//*
        if(**s == '%' || **s == '\0')
            break;*/
        if(**s == '%' || **s == '\0')
            break;
		write(1, *s, 1);
        (*s)++;
        i++;

	}
    return (i);
}
