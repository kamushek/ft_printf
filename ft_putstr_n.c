/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_n.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 22:34:19 by mkurchin          #+#    #+#             */
/*   Updated: 2017/04/03 22:34:21 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "ft_printf.h"

int		ft_putstr_n(char *arg, int len, t_var *ptr)
{
	int i;
	char *str;

    str = arg;
    i = 0;
    if (str == NULL && ptr->type == 's')
    {
        write(1, "(null)", 6);
        return(6);
    }
    else if (str == NULL && ptr->type == 'c')
        return (1);
	if(ptr->type != 0)
	{
		if (str == NULL)
			return (0);
		if (ptr->type == 'S' || ptr->type == 'C')
			return (ft_wht_byte((wchar_t *) arg));

        if (ptr->type == 'c' || ptr->type == 0)
        {
            write(1, &str, 1);
            return (1);
        }
        if (ptr->type == 0)
        {
            write(1, &str, 1);
            return (1);
        }
		while (i < len)
		{
			write(1, str, 1);
			str++;
			i++;
		}
		return (i);
	}
    else
    {
        if (str == NULL)
            return (0);
        write(1, str, 1);
        i++;
      /*  while(*str != '\0')
        {
            write(1, str, 1);
            str++;
            i++;
        }*/
        return (i);


    }
	return (0);
}
