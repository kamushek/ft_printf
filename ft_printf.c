/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 22:22:09 by mkurchin          #+#    #+#             */
/*   Updated: 2017/01/27 22:22:11 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "locale.h"
//#include "limits.h"


int 	ft_srch_dot(char *str)
{
	char *ptr;
	ptr = str;

	while(ft_isdigit(*ptr))
		ptr--;
	if (*ptr == '.')
		return (0);
	else
		return (1);
}

int 	ft_srch_end_zero(char *str)
{
	char *ptr;
	ptr = str;

	while(ft_isdigit(*ptr))
		ptr++;
	if (*ptr >= '0' && *ptr <= '9')
			return (1);
	else
			return (0); // width
}

int 	ft_search_type(const char *format)
{
	char *type = "sSpdDioOuUxXcC";

	while (*type != '\0')
	{
		if (*type == *format)
			return (1);
		type++;
	}
	return (0);
}


void	ft_fill_zero(t_var *ptr)
{
	int *p;
	int i;
	
	p = &(ptr->flag);
	i = -1;
	while(++i < 11)
		p[i] = 0;
}
int			ft_handle(const char **format, t_var *ptr)
{
	if (**format == '%' && *((*format)+1) == '%')
	{
		ft_putstr2(format, ptr);
		return (0);
	}
    if(**format == '%' && *((*format)+1) == '\0')
        return (1);
	if(**format == '%')
        (*format)++;
    return (1);
}

int			ft_printf(const char *format, ...)
{
	int 	i;
	t_var *ptr;
	void *var;
	int print_chr;

	print_chr = 0;
  	ptr = (t_var*)malloc(sizeof(t_var));
	ft_fill_zero(ptr);
	i = 0;
	if (format == NULL)
        ft_putstr2(&format, ptr);
    print_chr = ft_putstr2(&format, ptr);
	va_list ap;
	va_start(ap, format);
		while (*format != '\0') // count_arg
		{
           // if (*format == '%')
           // {
            //
            ft_handle(&format, ptr);

            if (ft_parse((char *)format, ptr))// retrun 1 when is case type modificator
            {
                    var = (void*)va_arg(ap, void*);
                    format++;
                   // printf("dsfsdf");
                   // printf("--->%lld", (long long)var);
                    //unsigned long long bb = (unsigned long long)var;
                    print_chr += ft_print_mod(format, ptr, var);
                    print_chr += ft_putstr2(&format, ptr);
                    ft_fill_zero(ptr);

            }
            else if (ft_srch(format, ptr)) // return 1 when is not modificator so another output
            {
                    print_chr += ft_print_mod(format, ptr, var);
                    format++;
                    print_chr += ft_putstr2(&format, ptr);
                    ft_fill_zero(ptr);
            }

            if (*format == '\0')
                return (print_chr);
            if (*format == '\0')
            {
                ft_fill_zero(ptr);
                return (print_chr);
            }

			format++;
			i++;
		}
		va_end(ap);
	return (print_chr);
}
