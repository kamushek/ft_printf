
#include "ft_printf.h"

/*
uintmax_t ft_overfall_u(uintmax_t num, t_var *ptr)
{
    int int_n;
    char chr_n;

    int_n = 0;
    chr_n = 0;
    if (ptr->type == 'd' && ptr->length != 216 && ptr->length != 'l'
        && ptr->length != 'j')
    {
        if (num > UCHAR_MAX && ptr->length == 208)
        {

            chr_n = (unsigned char)num;

            if (chr_n < UCHAR_MAX)
                num = chr_n * -1;
            else if (chr_n < 0 && chr_n > UCHAR_MAX)
                num = chr_n * -1;
            ptr->sign = 1;
        }
        else if (num > USHRT_MAX && ptr->length == 'h')
        {
            int_n = num;
            if (int_n < SHRT_MIN)
                num = int_n * -1;
            else if (int_n < 0 && int_n > SHRT_MIN)
                num = int_n * -1;
            ptr->sign = 1;
        }
        else if(num > INT_MAX )
        {
            int_n = num;
            if (int_n < INT_MIN)
                num = int_n * -1;
            else if (int_n < 0 && int_n > INT_MIN)
                num = int_n * -1;
            ptr->sign = 1;
        }

    }
    //num = nn;
    return (num);
}
*/
int ft_is_overfall(void *arg, t_var *ptr)
{
    short bb;
    if ((int)arg > SCHAR_MAX && ptr->length == 208)
        return (1);
    if ((ptr->type == 'd' || ptr->type == 'i') && ptr->length != 216 && ptr->length != 'l'
        && ptr->length != 'j' && ptr->length != 'z')
    {   bb = (short)arg;
        if ((short)arg >= SHRT_MIN && (short)arg < 0 && ptr->length == 'h')
            return (1);
        if (((short)arg > SHRT_MAX && ptr->length == 'h'))
           return(1);
        if((long long)arg > INT_MAX )
           return(1);

    }
    if ((ptr->type == 'd' || ptr->type == 'i') && ptr->length == 216 )
    {
        if((unsigned long long)arg > LONG_LONG_MAX)
            return (1);
    }
        return(0);


    return (0);
}
uintmax_t ft_hndl_overfall(void *num, t_var *ptr)
{
    int int_n;
    int chr_n;
    short bb;
//    unsigned long long unl_n;
    unsigned short chr_n2;
    uintmax_t sum;

    sum = 0;
    int_n = 0;
    chr_n = 0;
    bb = (short)num;
    if ((short)num >= SHRT_MIN && (short)num < 0 && ptr->length == 'h')
    {
        ptr->sign = 1;
        sum = ((short)num) * -1;
        return (sum);
    }
    if ((ptr->type == 'd' || ptr->type == 'i') && ptr->length == 216 )
    {

        //unsigned long long consta = (unsigned long long)LONG_LONG_MAX;
        if((long long)num >= LONG_LONG_MIN)
        {
            ptr->sign = 1;
            sum = (unsigned long long)num;
            return(sum);
        }


    }
    if ((ptr->type == 'd' || ptr->type == 'i') && ptr->length != 216 && ptr->length != 'l'
        && ptr->length != 'j' && ptr->length != 'z')
    {
        if ((int)num > SCHAR_MAX && ptr->length == 208)
        {

            chr_n = (unsigned char)num;
            chr_n2 = (unsigned char)num;

            if (chr_n < SCHAR_MIN)
                sum = (unsigned char)chr_n * -1;
            else if (chr_n < 0 && chr_n > SCHAR_MIN)
                sum = chr_n * -1;
            ptr->sign = 1;
            sum = chr_n;
            return(sum);
        }
        if ((short)num > SHRT_MAX && ptr->length == 'h')
        {
            int_n = (short)num;
            if (int_n < SHRT_MIN)
                sum = int_n * -1;
            else if (int_n < 0 && int_n > SHRT_MIN)
                sum = int_n * -1;
            ptr->sign = 1;
            return(sum);
        }
        else if((long long)num > INT_MAX )
        {
            int_n = (int)num;
            if (int_n <= INT_MIN)
                sum = (unsigned int)int_n * -1;
            else if (int_n < 0 && int_n > INT_MIN)
                sum = int_n * -1;
            ptr->sign = 1;

            return(sum);
        }

    }
    return (0);
}
int ft_handl_wdth_num(t_var *ptr, int len, char chr)
{
		int sum;
		int i;
		
		i = -1;
		sum = (ptr->width < len) ? 0 : ptr->width - len;
		while (++i < sum)
			write(1, &chr, 1);
		return (i);
}
int ft_handl_precis_num(t_var *ptr, int cnt_digit)
{
	int i;
	int len;

	i = -1;
	len = (cnt_digit > ptr->precision) ? 0 : ptr->precision - cnt_digit;
	while (++i < len)
		write(1, "0", 1);
	return (i);
}

int		ft_is_base(t_var *ptr)
{
	int base;
	base = 0;
	if (ptr->type == 'd' || ptr->type == 'D' || 
		ptr->type == 'u' || ptr->type == 'U' || 
		ptr->type == 'i')
		base = 10;
	if (ptr->type == 'o' || ptr->type == 'O')
		base = 8;
	if (ptr->type == 'x' || ptr->type == 'X' || ptr->type == 'p')
		base = 16;

	return (base);
}
int ft_is_unsign_type(t_var *ptr)
{
    if (ptr->type == 'o' || ptr->type == 'u'
        ||ptr->type == 'x' ||ptr->type == 'X')
        return (1);
    return (0);
}
int ft_is_sign(t_var *ptr)
{
	int fl;

	fl = 0;
	fl = (ptr->sign == 1) ?  1 : fl ;
    fl = ((ptr->type == 'x' || ptr->type == 'X') && ptr->sign == 1 ) ? 0 : fl;
    fl = (ptr->plus == '+' && ptr->sign == 0) ? 1 : fl;
	fl = (ptr->diez == '#' && (ptr->type == 'x' || ptr->type == 'X')) ? 2 : fl;
	fl = ((ptr->diez == '#' && ptr->type == 'p') || ptr->type == 'p') ? 2 : fl;
	fl = (ptr->diez == '#' && (ptr->type == 'o' || ptr->type == 'O')) ? 1 : fl;
    fl += (ptr->space == ' ' && ptr->sign == 0) ? 1 : 0;

    return (fl);
}
int ft_handl_sign(t_var *ptr, uintmax_t num)
{

	if (ptr->sign == 1 && !(ft_is_unsign_type(ptr)))
    {
        write(1, "-", 1);
        return (1);
    }
    if (ptr->plus == '+' && ptr->sign == 0 && (ptr->type == 'd' || ptr->type == 'i'))
    {
        write(1, "+", 1);
        return (1);
    }
	if (ptr->diez == '#' && (ptr->type == 'x' || ptr->type == 'p') && num != 0)
    {
        write(1, "0x", 2);
        return (2);
    }
	if (ptr->diez == '#' && ptr->type == 'X')
    {
        write(1, "0X", 2);
        return (2);
    }
	if (ptr->diez == '#' && (ptr->type == 'o' || ptr->type == 'O'))
    {
        write(1, "0", 2);
        return (1);
    }
    if (ptr->type == 'p' )
    {
        write(1, "0x", 2);
	    return (2);
    }
    return (0);
}
int ft_cnt_digit(unsigned long long num, int base)
{
	int i;

    i = 1;
	while (num /= base)
		i++;
	return (i);
}

uintmax_t	ft_cast_long(void *arg, t_var *ptr)
{
	uintmax_t n;

    n = 0;

    if (ptr->type == 'i' || ptr->type == 'd' || ptr->type == 'p')
    {
        if(ft_is_overfall(arg, ptr))
            return (ft_hndl_overfall(arg, ptr));
        if (ptr->length == 'l')
            return ((long)arg);
        if (ptr->length == 216)
            return ((long long)arg);
        if (ptr->length == 'h')
            return ((short)arg);
        if (ptr->length == 208)
            return (n = (signed char)arg);
        if (ptr->length == 'j')
            return ((intmax_t)arg);
        if (ptr->length == 'z')
            return ((size_t)arg);
        if (ptr->type == 'p')
            return ((long long)arg);

        if (ptr->length == 0)
            return ((int)arg);
       /* if (n == 0)
            return ((uintmax_t)arg);*/
        return (n);
    }
    else if (ptr->type == 'x' || ptr->type == 'X' ||
            ptr->type == 'o' || ptr->type == 'u')
    {
        if (ptr->length == 'h')
            return ((unsigned short)arg);
        if (ptr->length == 208)
            return ((unsigned char)arg);
        if (ptr->length == 'l')
            return ((unsigned long)arg);
        if (ptr->length == 216)
            return ((unsigned long long)arg);
        if (ptr->length == 'j')
            return ((uintmax_t)arg);
        if (ptr->length == 'z')
            return ((size_t)arg);
        if (ptr->length == 0 && (ptr->type == 'x' || ptr->type == 'X'))
            return((unsigned int)arg);
        if (ptr->length == 'h')
            return((unsigned short)arg);

        else
            n = (n == 0) ? (unsigned int)arg : n;
       // n = ft_overfall_u(arg, ptr);

    }
	return (n);
}

int ft_handle_fl_num(void *arg, t_var *ptr)
{
	int cnt_digit;
	int str_len;
    int str_len_sgn;
	int print_chr;
    uintmax_t num;
	int base;

    str_len_sgn = 0;
	base = ft_is_base(ptr);
	print_chr = 0;
	num = ft_cast_long(arg, ptr);
    //long long *test =
   	cnt_digit = ft_cnt_digit(num, base);
	str_len = (cnt_digit > ptr->precision) ? cnt_digit : ptr->precision;
    str_len = (ptr->precision == 0 && ptr->dote == 1) ? 0 : str_len;
    cnt_digit = (ptr->precision == 0 && ptr->dote == 1 && num == 0) ? 0 : cnt_digit;
    str_len_sgn = ft_is_sign(ptr) + str_len;

    //str_len += (ft_is_unsign_type(ptr) == 0) ? ft_is_sign(num, ptr) : 0;
	if (ptr->flag == '-')
	{
        print_chr += ft_handl_sign(ptr, num);
		print_chr += ft_handl_precis_num(ptr, cnt_digit);
		print_chr += ft_putstr_n(ft_itoa_base(num, base, ptr), cnt_digit, ptr);
        if (ptr->width != 0)
			print_chr += ft_handl_wdth_num(ptr, str_len_sgn, ' ');
	}
	else if (ptr->plus == '+')
	{
        if (ptr->width != 0 && ptr->zero == 0 )
			print_chr += ft_handl_wdth_num(ptr, str_len_sgn, ' ');
        print_chr += ft_handl_sign(ptr, num);
        if (ptr->width != 0 && ptr->zero != 0 && ptr->plus != 0)
			print_chr += ft_handl_wdth_num(ptr, str_len_sgn, '0');
        print_chr += ft_handl_precis_num(ptr, cnt_digit);
		print_chr += ft_putstr_n(ft_itoa_base(num, base, ptr),  cnt_digit, ptr);
	}
	else if (ptr->zero == '0')
	{
        if (ptr->sign == 0 && ptr->space == ' ')
        {
            write(1, " ", 1);
            print_chr++;
        }
            print_chr += ft_handl_sign(ptr, num);
            if (ptr->width != 0 && ptr->precision == 0)
				print_chr += ft_handl_wdth_num(ptr, str_len_sgn, '0');
			else if (ptr->precision != 0) // if we have precignore 0 as flag so output ' ' 
				print_chr += ft_handl_wdth_num(ptr, str_len_sgn, ' ');
                print_chr += ft_handl_precis_num(ptr, cnt_digit);
        print_chr += ft_putstr_n(ft_itoa_base(num, base, ptr),  cnt_digit, ptr);
	}
	else if (ptr->space == ' ')
	{
		if (ptr->sign == 0 && (ptr->type == 'd' || ptr->type == 'i'))
		{
			write(1, " ", 1);
			print_chr++;
		}
        if (ptr->width != 0 && ptr->precision == 0)
            print_chr += ft_handl_wdth_num(ptr, str_len_sgn, ' ');
        else if (ptr->precision != 0)
            print_chr += ft_handl_wdth_num(ptr, str_len_sgn, ' ');
        print_chr += ft_handl_sign(ptr, num);
        print_chr += ft_handl_precis_num(ptr, cnt_digit);
        print_chr += ft_putstr_n(ft_itoa_base(num, base, ptr), cnt_digit, ptr);

        /*if (ptr->width != 0 && ptr->precision == 0)
                print_chr = ft_handl_wdth_num(ptr, str_len_sgn, '0');
        else if (ptr->precision != 0)
        {
            ft_handl_precis_num(ptr, cnt_digit);
                print_chr = ft_handl_wdth_num(ptr, str_len_sgn, ' ');
        }
        print_chr += ft_handl_sign(ptr, num);
        print_chr += ft_putstr_n(ft_itoa_base(arg, base, ptr),  cnt_digit, ptr);*/

	}
	else if (ptr->diez == '#')
	{
		if (ptr->width != 0 && ptr->precision == 0)
				print_chr = ft_handl_wdth_num(ptr, str_len_sgn, ' ');
		else if (ptr->precision != 0)
				print_chr += ft_handl_wdth_num(ptr, str_len_sgn, ' ');
		ft_handl_precis_num(ptr, cnt_digit);
        print_chr += ft_handl_sign(ptr, num);
		print_chr += ft_putstr_n(ft_itoa_base(num, base, ptr),  cnt_digit,ptr);

	}
	else if (ptr->flag == 0)
	{

        if (ptr->width != 0 && ptr->precision == 0)
				print_chr = ft_handl_wdth_num(ptr, str_len_sgn, ' ');
        else if (ptr->precision != 0)
				print_chr = ft_handl_wdth_num(ptr, str_len_sgn, ' ');
            print_chr += ft_handl_sign(ptr, num);
		print_chr += ft_handl_precis_num(ptr, cnt_digit);
		print_chr += ft_putstr_n(ft_itoa_base(num, base, ptr), cnt_digit, ptr);

	}
	return (print_chr);
}