/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_mod.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/22 13:00:48 by mkurchin          #+#    #+#             */
/*   Updated: 2017/03/22 13:00:49 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"



/*void ft_show_struct(t_var *ptr)
{
	printf("diez->%i\n",ptr->diez);
	printf("zero->%i\n",ptr->zero);
	printf("space->%i\n",ptr->space);
	printf("flags->%i\n",ptr->flag);
	printf("width->%i\n",ptr->width);
	printf("precision->%i\n",ptr->precision);
	printf("length->%i\n",ptr->length);
	printf("type->%c\n",ptr->type);
	
}*/




int ft_print_mod(const char *format, t_var *ptr, void *var)
{
	int len;
   // uintmax_t arg;
    //if (var == NULL)
   // arg = (signed char)var;
    /*if ((intmax_t)var < 0 && !(ft_is_unsign_type(ptr)))
    {    ptr->sign = 1;
        arg = (((long long)var) * -1);
    }
    else
    {
        arg = (uintmax_t)var;
    }*/

	len = 0;
    //unsigned long long num = *(unsigned long long *)arg;

	/*if (*(long long*)&arg >127 && (ptr->type != 'd' || ptr->type != 'i')
		return (0);*/
	if (ptr->type == 's' ||  ptr->type == 'S')
		return (ft_handle_fl_str(var, ptr));
	if (ptr->type == 'c')
		return (ft_handle_fl_str(var, ptr));
    if (ptr->type == 'C')
        return (ft_print_wchar(var));
	if (ptr->type == 'd' || ptr->type == 'D' || 
		ptr->type == 'x' || ptr->type == 'X' || 
		ptr->type == 'o' || ptr->type == 'O' || 
		ptr->type == 'u' || ptr->type == 'U' || 
		ptr->type == 'i' || ptr->type == 'p')
	{
		return (ft_handle_fl_num(var, ptr));
	}
	if (ptr->type == 0)
	{
        //arg = (void *)format;
        return (ft_handle_fl_str((void *)format, ptr));
	}
	return (0);
	
}
