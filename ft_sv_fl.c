/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sv_fl.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 18:14:00 by mkurchin          #+#    #+#             */
/*   Updated: 2017/03/29 18:14:05 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_sv_fl(char *fl, int i, t_var *ptr)
{
	int *p;
	int w;

	w = 0;
	p = &(ptr->flag);
	if (ft_isdigit(*fl) && ft_srch_dot(fl) == 1) // width become
	{
		while (ft_isdigit(*fl))
			fl--;
		fl++;
		ptr->zero = (*fl == '0') ? '0' : ptr->zero ;
		w = ft_atoi(fl);
		if (w != 0)
			ptr->width = w;
	}
	else if (ft_isdigit(*fl) && !(ft_srch_dot(fl))) // width
		return (0);
	else if (*fl == '.')
	{
		ptr->precision = ft_atoi(++fl);
		ptr->dote = 1;
	}
	else if ((*fl == 'h' || *fl == 'l')
			 && (*(fl + 1) == *fl || *(fl - 1) == *fl)) // double
	{
			if (ptr->length != 104 && ptr->length != 108)
				ptr->length = ((*fl) * 2);
	}
	else
	{
		if (!(ft_srch(fl, ptr)))
			ft_add_fl(fl, &p[i], ptr);
	}
	return (0);
}


