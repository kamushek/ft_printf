/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wchar.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 22:35:34 by mkurchin          #+#    #+#             */
/*   Updated: 2017/04/03 22:35:35 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "ft_printf.h" 

void	ft_conv_2b(unsigned char *ptr, int *arr)
{
	int tmp;

	tmp = *ptr >> 6;
	*ptr <<= 2;
	arr[0] = (*ptr >>= 2) + MASK1;
	ptr++;
	*ptr <<= 3;
	arr[1] = ((*ptr >>= 1) + tmp) + MASK2;
}
void	ft_conv_3b(unsigned char *ptr, int *arr)
{
	int tmp;

	tmp = *ptr >> 6;
	*ptr <<= 2;
	arr[0] = (*ptr >>= 2) + MASK1;
	ptr++;
	arr[2] = (*ptr >> 4);
	*ptr <<= 4;
	arr[1] = (*ptr >>= 2) + tmp + MASK1;
	ptr++;
	*ptr <<= 4;
	arr[2] += (*ptr) + MASK3;
}
void	ft_conv_4b(unsigned char *ptr, int *arr)
{
	int tmp;

	tmp = (*ptr) >> 6; // remain
	*ptr <<= 2;
	arr[0] = (*ptr >> 2) + MASK1;
	ptr++;
	arr[2] = (*ptr >> 4);
	*ptr <<= 4;
	arr[1] += (*ptr >> 2) + tmp + MASK1;
	ptr++;
	arr[3] = *ptr >> 4;
	*ptr <<= 6;
	arr[2] += (*ptr >>= 2) + MASK1;
	ptr++;
	*ptr <<= 4;
	arr[3] += (*ptr >>= 4) + MASK4;
}
void	ft_print_utf8(int *c)
{
	int i;

	i = 4;
	while (i--)
		write(1, &c[i],1);
}

size_t ft_count_utf8(wchar_t *str)
{
	size_t i;

    i = 0;
    if (*str <= 127)
        i = 1;
    else if (*str <= 2047)
        i = 2;
    else if (*str <= 65535)
        i = 3;
    else if (*str <= 4194303)
        i = 4;
	return (i);
}

void	ft_hndl_msk(wchar_t *str)
{
	int arr[5];
	//unsigned char tmp;
	int test = (int)*str;
	unsigned char *ptr = (unsigned char *)&test;
	
	ft_bzero(arr, sizeof(int) * 5);
	//tmp = *ptr;
	if (*str <= 127)
		arr[0] = *ptr;
	else if (*str <= 2047)
		ft_conv_2b(ptr, arr);
	else if (*str <= 65535)
		ft_conv_3b(ptr, arr);
	else if (*str <= 4194303)
		ft_conv_4b(ptr, arr);
	ft_print_utf8(arr);
}

int		ft_wht_byte(wchar_t *str)
{
	while (*str != '\0')
	{
		ft_hndl_msk(str);
		str++;
	}
	return (0);
}

int	ft_print_wchar(void *str)
{
    int arr[5];
    int i;

    i = 0;
    int test = (int)str;
    unsigned char *p = (unsigned char *)&test;

    ft_bzero(arr, sizeof(int) * 5);
    if ((int)str <= 127)
    {
        arr[0] = *p;
        i = 1;
    }
    else if ((int)str <= 2047)
    {
        ft_conv_2b(p, arr);
        i = 2;
    }
    else if ((int)str <= 65535)
    {
        ft_conv_3b(p, arr);
        i = 3;
    }
    else if ((int)str <= 4194303)
    {
        ft_conv_4b(p, arr);
        i = 4;
    }
    ft_print_utf8(arr);
    return (i);
}

