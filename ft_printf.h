/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkurchin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 22:21:34 by mkurchin          #+#    #+#             */
/*   Updated: 2017/01/27 22:21:35 by mkurchin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdarg.h>
#include <inttypes.h>
#include <limits.h>


#define MASK1 128
#define MASK2 192
#define MASK3 224
#define MASK4 240

#define BUFF_SIZE 9000

char *spec_form[6];

typedef struct 	s_var {

	int		flag;
	int		width;
	int		precision;
	int		length;
	int		type;
	int		space;
	int		zero;
	int		diez;
    int		plus;
    int     sign;
    int     dote;
} 				t_var;

int				ft_printf(const char *format, ...);
void			ft_add_fl(char *fl, int *p, t_var *ptr);
uintmax_t		ft_cast_long(void *arg, t_var *ptr);
void			ft_bzero(void *s, size_t n);
int             ft_handle_fl_str(void *arg, t_var *ptr);
int 			ft_handle_fl_num(void * arg, t_var *ptr);
int				ft_handl_wdth_num(t_var *ptr, int len, char chr);
int 			ft_handle_string(const char *format, int num);
int             ft_handl_sign(t_var *ptr, uintmax_t num);
char			*ft_itoa_base(uintmax_t arg, int base, t_var *ptr);
int             ft_is_sign(t_var *ptr);
int 			ft_handl_precis_num(t_var *ptr, int len);
void			ft_hndl_msk(wchar_t* str);
int				ft_parse(char *format, t_var *ptr);
int             ft_print_mod(const char *format, t_var *ptr, void *arg);
void			ft_print_utf8(int *c);
int				ft_putstr2(const char **s, t_var *ptr);
int				ft_putstr_n(char *s, int len, t_var *ptr);
int				ft_srch_mask(int b, unsigned char *var1);
size_t			ft_count_utf8(wchar_t *str);
char			*ft_strrjoin(char *s1, char const *s2);
int				ft_search_spec(const char *format);
int				ft_simple_string(const char *format);
int				ft_sv_fl(char *fl, int i, t_var *ptr);
//void			ft_show_struct(t_var *ptr);
size_t 			ft_strlen_wchr(void *arg, t_var *ptr);
int 			ft_srch_dot(char *str);
int 			ft_srch_mask(int b, unsigned char *var1);
int				ft_wht_byte(wchar_t *arg);
void			ft_bzero(void *memptr, size_t num);
int				ft_isdigit(int c);
void			ft_putstr(char const *s);
void			ft_putchar(int c);
int				ft_atoi(const char *str);
char			*ft_strchr(const char *s, int c);
size_t			ft_strlen(char *s);
int				ft_srch(const char *format, t_var *ptr);
int             ft_is_unsign_type(t_var *ptr);
int             ft_print_wchar(void *str);
